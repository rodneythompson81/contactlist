module.exports = function (grunt) {
    // Project configuration.
    'use strict';
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            configFiles: {
                files: [ 'UsersAdminDocumentscontactform/index.html' ],
                options: {
                    reload: true
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 9009,
                    protocol: 'http',
                    host: 'localhost',
                    base: '.',
                    directory: 'null',
                    open: 'true'
                }
            }
        },
        sass: {                              
             dist: {                            
                files: {                         
                   'main.css': 'main.scss'
                }
             }
        },
        karma: {
            options: {
                configFile: '/test/karma.conf.js',
                browsers: ['PhantomJS'],
                singleRun: true
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('default', ['connect', 'watch', 'sass', 'karma']);
};