module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../../../',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'chai', 'chai-as-promised', 'sinon-chai', 'jasmine-jquery', 'jasmine'],

        plugins: [
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-mocha',
            'karma-chai-plugins',
            'karma-jasmine-jquery',
            'karma-jasmine'
        ],

        // list of files / patterns to load in the browser
        files: ['test/unit/*.js'],

        // list of files to exclude
        exclude: [],


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome', 'PhantomJS', 'Firefox'],

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // How long does Karma wait for a browser to reconnect (in ms)
        browserDisconnectTimeout: 60000,

        // Increase how long we wait for PhantomJS before giving up.
        browserNoActivityTimeout: 60000,

        // Timeout for capturing a browser (in ms)
        captureTimeout: 60000,

        // The number of disconnections tolerated
        browserDisconnectTolerance: 3
    });
};

