describe('contact controller', function($controller) {
var scope, getController, testRootScope;
    beforeEach(function() {
    module('contactList');
      inject(function($controller){
        getController = function() {
            return $controller('contactController', {
                '$scope': scope,
                '$rootScope': testRootScope
            });
        };
      });

        getController();

        testRootScope.$digest();


    });

        it('upon loading contacts should npt be empty', function() {
            expect(scope.contacts).to.not.be.empty;
        });

        it('upon loading contacts should npt be empty', function() {
            expect(scope.state).toMatch("close");
    });

});
