angular.module('contactList',['ngStorage']).controller('contactController', function($scope, $localStorage,
    $sessionStorage){

     var init = function(){
         $storage = $localStorage.localData;
         if($localStorage.localData){
             $scope.contacts = $localStorage.localData;
            }else{
             $scope.contacts = [
                 {'type': 'Executive', 'name': 'Ann Brown', 'Title': 'CEO', 'Phone': '(512)-462-5555', 'Ext':'', 'Fax':'(512)456-5555', 'email':'executive'},
                 {'type': 'Inmar AR', 'name': 'Mary Smith', 'Title': 'Lorem Ipsum', 'Phone': '(512)-462-5555', 'Ext':'', 'Fax':'(512)456-5555', 'email':'Inmar AR'},
                 {'type': 'Executive', 'name': 'John Doe', 'Title': 'Dolor Sit', 'Phone': '(512)-462-5555', 'Ext':'', 'Fax':'(512)456-5555', 'email':'executive'},
                 {'type': 'Daily', 'name': 'John Doe', 'Title': 'Dolor sir amet', 'Phone': '(512)-462-5555', 'Ext':'', 'Fax':'(512)456-5555', 'email':'Daily'},
                 {'type': 'executive', 'name': 'John Doe', 'Title': 'Lorem Ipsum', 'Phone': '(512)-462-5555', 'Ext':'', 'Fax':'(512)456-5555', 'email':'Other'}
             ];
         }
          //console.log($localStorage.localData);
          $('#coll').collapse("show");
          $scope.state = 'Close';
          $scope.icon = 'fa fa-angle-down';
      };
      
     $scope.submit = function(){
         var newValues = {
             'type':$scope.type, 'name':$scope.name, 'Title':$scope.Title, 'Phone':$scope.phone, 'Ext':$scope.ext, 'fax':$scope.fax, 'email':$scope.email
         };
             $scope.contacts.push(newValues);
             $localStorage.$reset();
             $localStorage.localData = $scope.contacts;
             $('#addUser').modal("hide");
             location.reload();
     };
     
     $scope.deleteUser = function(index){
         swal({   
             title: "Are you sure?",   
             text: "You will not be able to recover this Contact!",   
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#DD6B55",   
             confirmButtonText: "Yes, delete it!",   
             closeOnConfirm: false }, function(){
             swal("Deleted!", "Your Contact has been deleted.", "success");
             $scope.contacts.splice(index, 1);
         });
     };
     
     $scope.collaspeState = function(){
         if($scope.state === 'Close'){
             $scope.state = 'Show Contacts'
         }else if($scope.state === 'Show Contacts'){
                 $scope.state = 'Close'
         }
     };
     
     init();
});